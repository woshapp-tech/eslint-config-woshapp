## Configuration
Some type-aware lint rules in @typescript-eslint require a TSConfig to exist. The project itself does not need to use typescript, but a tsconfig should be present.

Add the following to `eslint.config.js`:
```javascript
const baseConfig = require('eslint-config-woshapp')

module.exports = [
    ...baseConfig,
]
```

Add e.g. the following to `tsconfig.json`:
```json
{
    "compilerOptions": {
        "target": "es6",
        "module": "esnext",
        "moduleResolution": "node",
        "jsx": "react-jsx",
        "allowJs": true,
        "checkJs": true,
        "noEmit": true,
        "experimentalDecorators": true,
        "useDefineForClassFields": true,
        "allowSyntheticDefaultImports": true,
        "esModuleInterop": true,
        "sourceMap": true,
        "skipLibCheck": true,
        "lib": ["es2022", "dom"]
    },
    "exclude": ["node_modules"]
}
```

## Node rules

### Install
```
npm install --save-dev eslint-config-woshapp globals eslint eslint-plugin-import eslint-plugin-n eslint-plugin-promise eslint-plugin-unicorn@56 typescript @stylistic/eslint-plugin@3 @typescript-eslint/parser @typescript-eslint/eslint-plugin eslint-plugin-communist-spelling
```

### Usage
Add the following to `eslint.config.js`:

```javascript
const nodeConfig = require('eslint-config-woshapp/node')

module.exports = [
    ...nodeConfig,
]
```

## React rules

### Install
```
npm install --save-dev eslint-config-woshapp globals eslint eslint-plugin-import eslint-plugin-n eslint-plugin-promise eslint-plugin-unicorn@56 typescript @stylistic/eslint-plugin@3 @typescript-eslint/parser @typescript-eslint/eslint-plugin eslint-plugin-communist-spelling eslint-plugin-react eslint-plugin-decorator-position eslint-plugin-mobx eslint-plugin-react-hooks
```

### Usage
Add the following to `eslint.config.js`:

```javascript
const reactConfig = require('eslint-config-woshapp/react')

module.exports = [
    ...reactConfig,
]
```

## React Native rules

### Install
```
npm install --save-dev eslint-config-woshapp globals eslint eslint-plugin-import eslint-plugin-n eslint-plugin-promise eslint-plugin-unicorn@56 typescript @stylistic/eslint-plugin@3 @typescript-eslint/parser @typescript-eslint/eslint-plugin eslint-plugin-communist-spelling eslint-plugin-react eslint-plugin-decorator-position eslint-plugin-mobx eslint-plugin-react-hooks eslint-plugin-react-native eslint-plugin-unused-imports eslint-plugin-prettier
```

### Usage
Add the following to `eslint.config.js`:

```javascript
const reactNativeConfig = require('eslint-config-woshapp/react-native')

module.exports = [
    ...reactNativeConfig,
]
```
