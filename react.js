const globals = require('globals')
const reactPlugin = require('eslint-plugin-react')
const reactHooksPlugin = require('eslint-plugin-react-hooks')
const decoratorPositionPlugin = require('eslint-plugin-decorator-position')
const mobxPlugin = require('eslint-plugin-mobx')
const mainConfig = require('./index')

module.exports = [
    ...mainConfig,
    {
        languageOptions: {
            ecmaVersion: 'latest',
            sourceType: 'module',
            parserOptions: {
                ecmaFeatures: {
                    jsx: true,
                    legacyDecorators: true,
                    experimentalDecorators: true,
                },
            },
            globals: {
                ...globals.browser,
                process: 'readonly',
                module: 'readonly',
            },
        },
        plugins: {
            'react': reactPlugin,
            'react-hooks': reactHooksPlugin,
            'decorator-position': decoratorPositionPlugin,
            'mobx': mobxPlugin,
        },
        settings: {
            react: {
                version: 'detect'
            },
            componentWrapperFunctions: ['observer'],
            linkComponents: [
                { name: 'Link', linkAttribute: 'to' },
            ],
        },
        rules: {
            ...reactPlugin.configs.recommended.rules,
            ...reactPlugin.configs['jsx-runtime'].rules,
            ...reactHooksPlugin.configs.recommended.rules,
    
            'decorator-position/decorator-position': ['error', {
                properties: 'above',
                methods: 'above',
            }],

            'import/no-commonjs': 'error',
            'import/no-duplicates': 'error',

            'mobx/missing-make-observable': 'error',

            'react/self-closing-comp': 'error',
            'react/display-name': 'off',
            'react/prop-types': 'off',
            'react-hooks/exhaustive-deps': 'error',
            'react-hooks/rules-of-hooks': 'error',

            '@stylistic/jsx-child-element-spacing': 'error',
            '@stylistic/jsx-curly-brace-presence': ['error', 'never'],
            '@stylistic/jsx-curly-newline': ['error', {
                multiline: 'consistent',
                singleline: 'consistent'
            }],
            '@stylistic/jsx-curly-spacing': ['error', {
                when: 'never',
                children: true,
            }],
            '@stylistic/jsx-equals-spacing': ['error', 'never'],
            '@stylistic/jsx-indent': ['error', 4, {
                checkAttributes: true,
                indentLogicalExpressions: true,
            }],
            '@stylistic/jsx-indent-props': ['error', {
                indentMode: 4,
                ignoreTernaryOperator: true,
            }],
            '@stylistic/jsx-max-props-per-line': ['error', {
                maximum: 2,
                when: 'multiline',
            }],
            '@stylistic/jsx-pascal-case': 'error',
            '@stylistic/jsx-props-no-multi-spaces': 'error',
            '@stylistic/jsx-tag-spacing': ['error', {
                closingSlash: 'never',
                beforeSelfClosing: 'always',
                afterOpening: 'never',
                beforeClosing: 'never',
            }],
        },
    },
]
