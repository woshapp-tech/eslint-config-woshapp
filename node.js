const globals = require('globals')
const nPlugin = require('eslint-plugin-n')
const mainConfig = require('./index')

module.exports = [
    ...mainConfig,
    {
        languageOptions: {
            ecmaVersion: 2022, // Node 18
            sourceType: 'module',
            env: {
                node: true,
                mocha: true,
            },
            globals: {
                ...globals.node,
            },
        },
        plugins: {
            'n': nPlugin,
        },
        rules: {
            ...nPlugin.configs.recommended.rules,
            'n/handle-callback-err': ['error', 'err'],
        },
    },
]
