const reactNativePlugin = require('eslint-plugin-react-native')
const prettierPlugin = require('eslint-plugin-prettier')
const unusedImportsPlugin = require('eslint-plugin-unused-imports')
const reactConfig = require('./react')

module.exports = [
    ...reactConfig,
    {
        languageOptions: {
            ecmaVersion: 2022,
            sourceType: 'module',
            parserOptions: {
                ecmaFeatures: { jsx: true },
            },
        },
        settings: {
            'react-native/style-sheet-object-names': ['StyleSheet', 'ViewStyle', 'TextStyle', 'ImageStyle'],
        },
        plugins: {
            'react-native': reactNativePlugin,
            'prettier': prettierPlugin,
            'unused-imports': unusedImportsPlugin,
        },
        rules: {
            ...reactNativePlugin.configs.all.rules,
            ...prettierPlugin.configs.recommended.rules,
            'no-unneeded-ternary': 'error',
            'react/jsx-no-leaked-render': 'error',
            'react/jsx-uses-react': 'error',
            'react/jsx-uses-vars': 'error',
            '@typescript-eslint/no-unused-vars': 'error',
            'unused-imports/no-unused-imports': 'error',
            'unused-imports/no-unused-vars': [
                'error',
                { 'vars': 'all', 'varsIgnorePattern': '^_', 'args': 'after-used', 'argsIgnorePattern': '^_' }
            ],
            'prettier/prettier': 'warn',
            '@typescript-eslint/consistent-type-imports': 'warn',
            '@typescript-eslint/consistent-indexed-object-style': 'warn',

            // More Rules for future

            // 'class-methods-use-this': 'off',
            // 'no-return-await': 'off',
            // 'no-console': 'off',
            // 'react/destructuring-assignment': 'off',
            // 'import/prefer-default-export': 'off',
            // 'import/no-cycle': 'off',
            // 'no-shadow': 'off',
            // 'react/jsx-props-no-spreading': 'off',
            // 'no-use-before-define': 'off',
            // 'import/no-import-module-exports': 'off',
            // 'react/no-array-index-key': 'off',
            // 'no-alert': 'off',
            // 'no-param-reassign': 'off',
            // 'prefer-destructuring': 'off',
            // 'no-plusplus': 'off',
            // 'no-underscore-dangle': 'off',
            // 'no-unused-vars': 'off', // or '@typescript-eslint/no-unused-vars': 'off',

            '@typescript-eslint/no-explicit-any': 'warn',
            'no-restricted-imports': ['warn', { 'patterns': ['../*'] }],
            'import/order': [
                'warn',
                {
                    'groups': ['builtin', 'external', 'internal'],
                    'pathGroups': [
                        {
                            'pattern': 'react',
                            'group': 'external',
                            'position': 'before'
                        },
                        {
                            'pattern': 'react-native',
                            'group': 'external',
                            'position': 'after'
                        },
                        {
                            'pattern': '~store/**',
                            'group': 'internal',
                            'position': 'after'
                        },
                        {
                            'pattern': '~utils/**',
                            'group': 'internal',
                            'position': 'after'
                        },
                        {
                            'pattern': '~types/**',
                            'group': 'internal',
                            'position': 'after'
                        },
                        {
                            'pattern': '~api/**',
                            'group': 'internal',
                            'position': 'after'
                        },
                        {
                            'pattern': '~actions/**',
                            'group': 'internal',
                            'position': 'after'
                        },
                        {
                            'pattern': '~screens/**',
                            'group': 'internal',
                            'position': 'after'
                        },
                        {
                            'pattern': '~components/**',
                            'group': 'internal',
                            'position': 'after'
                        },
                        {
                            'pattern': './**',
                            'group': 'internal',
                            'position': 'after'
                        }
                    ],
                    'alphabetize': {
                        'order': 'asc',
                        'caseInsensitive': true
                    }
                }
            ]
        },
    },
]
