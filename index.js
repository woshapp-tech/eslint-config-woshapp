const eslint = require('@eslint/js')
const tseslint = require('@typescript-eslint/eslint-plugin')
const tsParser = require('@typescript-eslint/parser')
const stylisticPlugin = require('@stylistic/eslint-plugin')
const communistSpellingPlugin = require('eslint-plugin-communist-spelling')
const promisePlugin = require('eslint-plugin-promise')
const unicornPlugin = require('eslint-plugin-unicorn')
const importPlugin = require('eslint-plugin-import')

module.exports = [
    {
        languageOptions: {
            ecmaVersion: 'latest',
            sourceType: 'module',
            parser: tsParser,
            parserOptions: {
                ecmaFeatures: { jsx: true },
                project: './tsconfig.json',
            },
        },
        plugins: {
            // stylistic plugin covers both js and ts
            '@stylistic': stylisticPlugin,
            '@typescript-eslint': tseslint,
            'communist-spelling': communistSpellingPlugin,
            'promise': promisePlugin,
            'unicorn': unicornPlugin,
            'import': importPlugin,
        },
        rules: {
            ...eslint.configs.recommended.rules,
            ...tseslint.configs.recommended.rules,
            ...tseslint.configs['stylistic-type-checked'].rules,
            // TODO: enable?
            // ...stylisticPlugin.configs['recommended-flat'].rules,
            ...promisePlugin.configs.recommended.rules,
            ...unicornPlugin.configs.recommended.rules,
            ...importPlugin.flatConfigs.recommended.rules,

            '@stylistic/array-bracket-newline': ['error', 'consistent'],
            '@stylistic/array-bracket-spacing': 'error',
            '@stylistic/array-element-newline': ['error', 'consistent'],
            '@stylistic/arrow-spacing': ['error', {
                before: true,
                after: true,
            }],
            '@stylistic/brace-style': ['error', '1tbs'],
            '@stylistic/comma-dangle': ['error', {
                arrays: 'always-multiline',
                objects: 'only-multiline',
                imports: 'always-multiline',
                exports: 'always-multiline',
                functions: 'only-multiline',
            }],
            '@stylistic/comma-spacing': ['error', {
                before: false,
                after: true,
            }],
            '@stylistic/comma-style': ['error', 'last'],
            '@stylistic/computed-property-spacing': ['error', 'never'],
            '@stylistic/dot-location': ['error', 'property'],
            '@stylistic/eol-last': ['error', 'always'],
            '@stylistic/func-call-spacing': ['error', 'never'],
            '@stylistic/function-call-argument-newline': ['error', 'consistent'],
            '@stylistic/function-paren-newline': ['error', 'consistent'],
            '@stylistic/indent': ['error', 4, {
                SwitchCase: 1,
            }],
            '@stylistic/jsx-quotes': ['error', 'prefer-double'],
            '@stylistic/key-spacing': ['error', {
                beforeColon: false,
                afterColon: true,
                mode: 'strict',
            }],
            '@stylistic/keyword-spacing': ['error', {
                before: true,
                after: true,
            }],
            '@stylistic/linebreak-style': ['error', 'unix'],
            '@stylistic/lines-between-class-members': ['error', 'always', {
                exceptAfterSingleLine: true,
            }],
            '@stylistic/max-len': ['error', {
                code: 140,
                ignoreStrings: true,
                ignoreRegExpLiterals: true,
                ignoreUrls: true,
                ignoreTemplateLiterals: true,
                ignoreComments: true,
            }],
            '@stylistic/newline-per-chained-call': ['error', {
                ignoreChainWithDepth: 3,
            }],
            '@stylistic/no-extra-parens': ['error', 'all', {
                ignoreJSX: 'all',
                nestedBinaryExpressions: false,
            }],
            '@stylistic/no-floating-decimal': 'error',
            '@stylistic/no-mixed-operators': ['error', {
                groups: [
                    ['%', '**'],
                    ['%', '+'],
                    ['%', '-'],
                    ['%', '*'],
                    ['%', '/'],
                    ['/', '*'],
                    ['&', '|', '<<', '>>', '>>>'],
                    ['==', '!=', '===', '!==', '>', '>=', '<', '<='],
                    ['&&', '||'],
                    ['in', 'instanceof'],
                ],
                allowSamePrecedence: true,
            }],
            '@stylistic/no-multi-spaces': ['error', {
                ignoreEOLComments: false,
                exceptions: {
                    Property: false,
                    VariableDeclarator: false,
                    ImportDeclaration: false,
                },
            }],
            '@stylistic/no-multiple-empty-lines': ['error', {
                max: 1,
                maxBOF: 0,
                maxEOF: 0,
            }],
            '@stylistic/no-tabs': 'error',
            '@stylistic/no-trailing-spaces': 'error',
            '@stylistic/no-whitespace-before-property': 'error',
            '@stylistic/object-curly-newline': ['error', {
                consistent: true,
            }],
            '@stylistic/object-curly-spacing': ['error', 'always'],
            '@stylistic/object-property-newline': ['error', {
                allowAllPropertiesOnSameLine: true,
            }],
            '@stylistic/operator-linebreak': ['error', 'before'],
            '@stylistic/padded-blocks': ['error', 'never'],
            '@stylistic/quote-props': ['error', 'consistent-as-needed', {
                numbers: true,
            }],
            '@stylistic/quotes': ['error', 'single', {
                allowTemplateLiterals: true,
                avoidEscape: false,
            }],
            '@stylistic/rest-spread-spacing': ['error', 'never'],
            '@stylistic/semi': ['error', 'never'],
            '@stylistic/space-before-blocks': ['error', 'always'],
            '@stylistic/space-before-function-paren': ['error', {
                anonymous: 'never',
                named: 'never',
                asyncArrow: 'always',
            }],
            '@stylistic/space-in-parens': ['error', 'never'],
            '@stylistic/space-infix-ops': 'error',
            '@stylistic/space-unary-ops': 'error',
            '@stylistic/spaced-comment': ['error', 'always', {
                exceptions: ['*'],
                block: {
                    balanced: true,
                },
            }],
            '@stylistic/switch-colon-spacing': 'error',

            'array-callback-return': ['error', {
                allowImplicit: true,
            }],
            'curly': ['error', 'multi-line'],
            'default-case': ['error', {
                commentPattern: '^no default$',
            }],
            'default-param-last': 'error',
            'dot-notation': 'error',
            'eqeqeq': ['error', 'always', {
                null: 'ignore',
            }],
            'logical-assignment-operators': ['error', 'always', {
                enforceForIfStatements: true,
            }],
            'no-console': 'off',
            'no-else-return': 'error',
            'no-implicit-coercion': 'error',
            'no-nested-ternary': 'error',
            'no-return-assign': 'error',
            'no-throw-literal': 'error',
            'no-undef': 'error',
            'no-unneeded-ternary': ['error', {
                defaultAssignment: false,
            }],
            'no-unused-vars': 'off',
            'no-useless-concat': 'error',
            'no-useless-escape': 'error',
            'no-useless-rename': ['error', {
                ignoreDestructuring: false,
                ignoreImport: false,
                ignoreExport: false,
            }],
            'no-useless-return': 'error',
            'no-var': 'error',
            'object-shorthand': 'error',
            'one-var': ['error', {
                initialized: 'never',
                uninitialized: 'consecutive',
            }],
            'operator-assignment': ['error', 'always'],
            'prefer-const': 'error',
            'prefer-promise-reject-errors': 'error',
            'prefer-spread': 'error',
            'require-await': 'error',
            'sort-imports': ['error', {
                ignoreCase: false,
                ignoreDeclarationSort: true,
                ignoreMemberSort: false,
                memberSyntaxSortOrder: ['none', 'all', 'multiple', 'single'],
            }],
            'unicode-bom': ['error', 'never'],
            'yoda': 'error',

            'import/order': ['error', {
                groups: [
                    'builtin',
                    'external',
                    'internal',
                    // TODO: consider enabling
                    // 'unknown',
                    // 'parent',
                    // 'sibling',
                    // 'index',
                ],
            }],

            'unicorn/catch-error-name': ['error', {
                name: 'err',
            }],
            'unicorn/no-array-reduce': ['error', {
                allowSimpleOperations: true,
            }],
            'unicorn/switch-case-braces': ['error', 'avoid'],
            'unicorn/consistent-function-scoping': 'off',
            'unicorn/filename-case': 'off',
            'unicorn/new-for-builtins': 'off',
            'unicorn/no-abusive-eslint-disable': 'off',
            'unicorn/no-array-callback-reference': 'off',
            'unicorn/no-array-for-each': 'off',
            'unicorn/no-array-push-push': 'off',
            'unicorn/no-lonely-if': 'off',
            'unicorn/no-negated-condition': 'off',
            'unicorn/no-null': 'off',
            'unicorn/no-object-as-default-parameter': 'off',
            'unicorn/no-this-assignment': 'off',
            'unicorn/no-zero-fractions': 'off',
            'unicorn/numeric-separators-style': 'off',
            'unicorn/prefer-global-this': 'off',
            'unicorn/prefer-module': 'off',
            'unicorn/prefer-number-properties': 'off',
            'unicorn/prefer-object-from-entries': 'off',
            'unicorn/prefer-set-has': 'off',
            'unicorn/prefer-spread': 'off',
            'unicorn/prefer-string-raw': 'off',
            'unicorn/prefer-switch': 'off',
            'unicorn/prefer-ternary': 'off',
            'unicorn/throw-new-error': 'off',
            'unicorn/prevent-abbreviations': 'off',

            // enforces American spelling
            'communist-spelling/communist-spelling': ['error', {
                ignoreDestructuring: true,
            }],

            // @typescript-eslint/parser specific rules
            '@typescript-eslint/class-literal-property-style': 'off',
            '@typescript-eslint/consistent-type-imports': 'error',
            '@typescript-eslint/consistent-indexed-object-style': 'error',
            '@typescript-eslint/naming-convention': [
                'error',
                {
                    selector: 'default',
                    format: ['camelCase'],
                    filter: {
                        regex: '^(_|_id)$',
                        match: false
                    },
                },
                {
                    selector: 'variable',
                    modifiers: ['global'],
                    format: ['camelCase', 'UPPER_CASE'],
                },
                {
                    selector: 'import',
                    format: ['camelCase', 'PascalCase'],
                },
                // TODO: allow PascalCase only on UI component
                {
                    selector: ['variable', 'function', 'objectLiteralProperty', 'objectLiteralMethod'],
                    types: ['function'],
                    format: ['camelCase', 'PascalCase'],
                },
                {
                    selector: 'variable',
                    modifiers: ['destructured'],
                    format: null,
                },
                {
                    selector: ['typeLike', 'enumMember'],
                    format: ['PascalCase'],
                },
                {
                    selector: 'memberLike',
                    modifiers: ['private'],
                    format: ['camelCase'],
                    leadingUnderscore: 'require',
                },
                {
                    selector: [
                    'classProperty',
                    'objectLiteralProperty',
                    'typeProperty',
                    'classMethod',
                    'objectLiteralMethod',
                    'typeMethod',
                    'accessor',
                    'enumMember',
                    ],
                    modifiers: ['requiresQuotes'],
                    format: null,
                },
                // TODO: force prefix on booleans?
                // {
                //     selector: ['variable', 'parameter', 'property', 'accessor'],
                //     types: ['boolean'],
                //     format: ['camelCase', 'PascalCase', 'UPPER_CASE'],
                //     prefix: ['is', 'has', 'are', 'can', 'should', 'did', 'will', 'was', 'show'],
                //     filter: {
                //         /**
                //          * Ignore any string that begins with '$' (e.g. $exists) as well as some keywords that are used in third-party libraries.
                //          * For example, antd uses loading in components.
                //          *
                //          * TODO: fix case-insensitive
                //          */
                //         regex: '^\\$|(Loading|Filter|Open|Visible|Closable|Disabled)$|^(loading|closable|editMode|reload|recurseEverything|active|disabled|loadImmediately|sorter)$',
                //         match: false,
                //     },
                // },
                // TODO: force plural naming on arrays?
                // {
                //     selector: ['variable', 'parameter', 'property'],
                //     types: ['array'],
                //     format: ['camelCase', 'PascalCase', 'UPPER_CASE'],
                //     suffix: ['s', 'S', 'List', 'Array', 'Filter'],
                //     filter: {
                //         /**
                //          * Ignore any string that begins with '$' (e.g. $in) as well as some keywords that are used in third-party libraries.
                //          * For example, antd uses dataIndex in tables.
                //          */
                //         regex: '^\\$|^dataIndex$',
                //         match: false
                //     },
                // },
            ],
            '@typescript-eslint/no-empty-function': 'off',
            '@typescript-eslint/no-for-in-array': 'error',
            '@typescript-eslint/no-implied-eval': 'error',
            '@typescript-eslint/no-misused-promises': 'error',
            '@typescript-eslint/no-this-alias': 'off',
            '@typescript-eslint/no-unused-vars': 'off',
            '@typescript-eslint/await-thenable': 'error',
            '@typescript-eslint/ban-ts-comment': 'off',
            '@typescript-eslint/prefer-for-of': 'error',
            '@typescript-eslint/prefer-includes': 'error',
            '@typescript-eslint/prefer-nullish-coalescing': 'off',
            // TODO: enable this rule when strictNullChecks=true in tsconfig
            // '@typescript-eslint/prefer-nullish-coalescing': ['error', {
            //     ignoreConditionalTests: true,
            //     ignoreMixedLogicalExpressions: true,
            // }],
            '@typescript-eslint/prefer-optional-chain': 'error',
            '@typescript-eslint/prefer-string-starts-ends-with': 'error',
        },
    },
]
